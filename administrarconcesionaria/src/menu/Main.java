/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;


import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;
import sistema.*;
import usuarios.Cliente;
import usuarios.*;
/**
 *
 * @author Carlos
 */
public class Main {
    
    static Scanner textoIngresado = new Scanner(System.in);
    static Vendedor vendedor;
    static Cliente cliente;
    static Supervisor supervisor;
    static JefeTaller jefeTaller;
    public static void main(String[] args){
        Proceso proceso = new Proceso();
        //INICIO DEL PROGRAMA
        System.out.println("-----=====BIENVENIDO(A) A LA CONCESARIONARIA=====-----");
        //PROCESO PARA ARRANCAR MEDIANTE EL MENU INICIAR SESION O SALIR DEL SISTEMA
        procesoInicial();
        
//        System.out.println(opcion);
    }
    public static void procesoInicial(){
        boolean validador = true;
        String opcion;
        // EL WHILE ES USADO PARA EJECUTAR EL MENU TANTAS VECES SEA NECESARIA. HASTA QUE EL USUARIO
        //INGRESO EN ALGUNA DE LAS OPCIONES DEL MENU
        while(validador){
            //EL MENU INICIAL ES LA PARTE VISUAL EN DONDE EL USUARIO PODRA VER LAS OPCIONES DISPONIBLES
            menuInicial();
            opcion = textoIngresado.nextLine();
            //EL METODO VALIDAR OPCION ES EL ENCARGADO DE REVISAR SI LA OPCION INGRESADO POR EL USUARIO
            // SEA VALIDA
            if (validarOpcion(opcion)){
                if (opcion.equals("1")){
                    validador = false;
                    //MENU RESPONSABLE PARA QUE EL USUARIO PUEDA INGRESAR A LOGEARSE
                    menuIniciarSesion();
                }
                if (opcion.equals("2")){
                    //EL METODO PROCESO.GRABAR DATOS ES EL ENCARGADO DE REGISTRAR TODOS LOS CAMBIOS QUE SE REALICEN
//                    EN EL SISTEMA
                    Proceso.grabarDatos();
                    System.exit(0);
                }
            }else{
                System.out.println("******************************************************************************" 
                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
            }
        }
    }
    //MENU DEMOSTRATIVO 
    public static void menuInicial(){
        System.out.println("Eliga una de las siguientes opciones para comenzar a utilizar el sistema!"
                + "\n" + "1.- Iniciar Sesión"
                + "\n" + "2.- Salir"
                + "\n");
        System.out.print("Por favor, digite el número de la acción que desea realizar: ");
    }
    //MENU DEMOSTRATIVO AL MOMENTO DE INICIAR SESION
    public static void menuIniciarSesion(){
        String user = "";
        String pass = "";
        boolean validar = true;
        System.out.println("******************************************************************************");
        System.out.println("Inicio de sesión en el sistema concesionaria!");
        while(validar){
            System.out.print("Ingrese su usuario: ");
            user = textoIngresado.nextLine();
            System.out.print("Ingrese su contraseña: ");
            pass = textoIngresado.nextLine();
            System.out.println("Datos Ingresados, usuario: " + user + ", " + "contraseña: " + pass);
            if (existeUsuario(user, pass) == true){
                validar = false;
            }
        }
    }
    //EXISTE USUARIO REVISA QUE EL USUARIO QUE ESTA TRATANDO DE INICIAR SESION
    //ESTE REGISTRADO Y QUE LOS DATOS DE USUARIO Y CONTRASEñA QUE INGRESE SEAN CORRECTOS
    public static boolean existeUsuario(String user, String pass){
        ArrayList<String> users = SistemaConcesionaria.getListUsuariosRegistrados();
        String usuario;
        String contraseña;
        String tpCliente;
        //RECORRE LA LISTA CON TODOS LOS USUARIOS REGISTRADOS
        for (int i = 0; i < users.size(); i++){
            //LOS SEPARA MEDIANTE ", " CONSIDERANDOLOS COMO TOKENS
            StringTokenizer st = new StringTokenizer(users.get(i), ",");
            tpCliente = st.nextToken();
            usuario = st.nextToken();
            contraseña = st.nextToken();
            //COMPARA QUE SEAN CORRECTOS LOS DATOS INGRESADOS CON LOS DATOS GUARDADOS
            if (user.equals(usuario) & pass.equals(contraseña)){
                System.out.println("Usuario Encontrado: Bienvenid(@) " + usuario);
                if(tpCliente.equals("c")){
                    ArrayList<Cliente> listCli = SistemaConcesionaria.getListCliente();
                    for(Cliente cli : listCli){
                        if (cli.getUsername().equals(usuario)){
                            cliente = cli;
                        }
                    }
                    menuCliente();
                }
                //UNA VEZ IDENTIFICADO QUE USUARIO ES, PROCEDE A CREAR EL TIPO DE USUAERIO 
                // ES DECIR SI INICIA SESION UN VENDEDOR CREA UN VENDEDOR, SI INICIA UN CLIENTE CREA UN CLIENTE 
                // Y ASI SUCESIVAMENTE
                if(tpCliente.equals("v")){
                    ArrayList<Vendedor> listVen = SistemaConcesionaria.getListVendedor();
                    for(Vendedor ven : listVen){
                        if (ven.getUsername().equals(usuario)){
                            vendedor = ven;
                        }
                    }
                    menuVendedor();
                }
                if(tpCliente.equals("s")){
                    ArrayList<Supervisor> listSup = SistemaConcesionaria.getListSupervisor();
                    for(Supervisor sup : listSup){
                        if (sup.getUsername().equals(usuario)){
                            supervisor = sup;
                        }
                    }
                    menuSupervisor();
                }
                if(tpCliente.equals("j")){
                    ArrayList<JefeTaller> listJefe = SistemaConcesionaria.getListJefeTaller();
                    for(JefeTaller jefe : listJefe){
                        if (jefe.getUsername().equals(usuario)){
                            jefeTaller = jefe;
                        }
                    }
                    menuJefeTaller();
                }
                return true;
            }
        }
        System.out.println("El usuario/contraseña ingresado es incorrecto!");
        return false;
    }
    //MENU DEMOSTRATIVO DE CLIENTE
    public static void menuCliente(){
        System.out.print("------------------------------------------" + "\n" + 
                         "               MENU CLIENTE               " + "\n" +
                         "------------------------------------------" + "\n" + 
                         "Eliga una de las siguientes opciones:     " + "\n" + 
                         "1.- Consultar Stock" + "\n" +
                         "2.- Estado Solicitudes Cotización" + "\n" +
                         "3.- Solicitudes de Compra" + "\n" +  
                         "4.- Solicitar Mantenimiento" + "\n" +  
                         "5.- Estado Solicitudes Mantenimiento" + "\n" +  
                         "6.- Consultar Progreso" + "\n" +  
                         "7.- Regresar" + "\n" +
                         "8.- Salir" + "\n" +
                         "Por favor, digite el número de la acción que desea realizar: " 
                       );
        //INGRESA A LA RESPECTIVA OPCION QUE HAYA INGRESADO POR TECLADO EL USUARIO
        String opc = textoIngresado.nextLine();
        Cliente.procesoCliente(opc, cliente);
    }
    //MENU DEMOSTRATIVO VENDEDOR
    public static void menuVendedor(){
        System.out.print("------------------------------------------" + "\n" + 
                           "               MENU VENDEDOR              " + "\n" +
                           "------------------------------------------" + "\n" +
                           "Eliga una de las siguientes opciones:     " + "\n" + 
                           "1.- Consultar Stock" + "\n" +
                           "2.- Revisar solicitudes de cotización" + "\n" +
                           "3.- Regresar" + "\n" +
                           "4.- Salir" + "\n" +
                           "Por favor, digite el número de la acción que desea realizar: " 
                           );
        //INGRESA A LA RESPECTIVA OPCION QUE HAYA INGRESADO POR TECLADO EL USUARIO
        String opc = textoIngresado.nextLine();
        Vendedor.procesoVendedor(opc, vendedor);
    }
    //MENU DEMOSTRATIVO SUPERVISOR
    public static void menuSupervisor(){
        System.out.print("------------------------------------------" + "\n" + 
                           "              MENU SUPERVISOR             " + "\n" +
                           "------------------------------------------" + "\n" +
                           "Eliga una de las siguientes opciones:     " + "\n" + 
                           "1.- Revisar solicitudes de compra" + "\n" +
                           "2.- Regresar" + "\n" +
                           "3.- Salir" + "\n" +
                           "Por favor, digite el número de la acción que desea realizar: " 
                           );
        //INGRESA A LA RESPECTIVA OPCION QUE HAYA INGRESADO POR TECLADO EL USUARIO
        String opc = textoIngresado.nextLine();
        Supervisor.procesoSupervisor(opc, supervisor);
    }
    //MENU DEMOSTRATIVO JEFETALLER
    public static void menuJefeTaller(){
        System.out.print("------------------------------------------" + "\n" + 
                           "            MENU JEFE DE TALLER           " + "\n" +
                           "------------------------------------------"+ "\n" +
                           "Eliga una de las siguientes opciones:     " + "\n" + 
                           "1.- Entregar Vehículo" + "\n" +
                           "2.- Revisar Solicitudes Mantenimiento" + "\n" +
                           "3.- Administrar Vehículo en mantenimiento" + "\n" +
                           "4.- Regresar" + "\n" +
                           "5.- Salir" + "\n" +
                           "Por favor, digite el número de la acción que desea realizar: " 
                           );
        //INGRESA A LA RESPECTIVA OPCION QUE HAYA INGRESADO POR TECLADO EL USUARIO
        String opc = textoIngresado.nextLine();
        JefeTaller.procesoJefeTaller(opc, jefeTaller);
    }
    
    //VERIFICA QUE LO INGRESADO POR TECLADO POR PARTE DEL USUARIO ESTE CORRECTO Y PUEDA SER UTILZIADO
    //PARA MANEJARSE EN EL SISTEMA ES DECIR QUE INGRESE CORRECTAMENTE LO QUE SE LE PIDE
    //QUE EN ESTE OCASION SON SOLO NUMEROS CORRESPONDIENTES A LAS OPCIONES DE LOS MENUS
    public static boolean validarOpcion(String opcion){
        try{
            int op = Integer.parseInt(opcion);
            return true;
        }catch(Exception ex){
            return false;
        }
    } 
}
      
