/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.util.ArrayList;
import java.util.Scanner;
import sistema.SistemaConcesionaria;
import vehiculos.*;
import menu.Main;
import sistema.*;

/**
 *
 * @author Carlos
 */
public class Vendedor extends Usuario{
    static Scanner textoIngresado = new Scanner(System.in);
    //ATRIBUTOS
    private int idInterno;
    private ArrayList<ArrayList> solicitudCotizacion = new ArrayList();
    
    //CONSTRUCTOR
    public Vendedor(){
        super("", "", "", "");
    }
    public Vendedor(String user, String pass, String apellido, String nombre, int idInterno){
        super(user, pass, apellido, nombre);
        this.idInterno = idInterno;
    }
    
    //GETTERS
    public int getIdInterno(){
        return this.idInterno;
    }
    public ArrayList<ArrayList> getSolicitudCotizacion(){
        return this.solicitudCotizacion;
    }
    
    //SETTERS
    public void setIdInterno(int idInterno){
        this.idInterno = idInterno;
    }
    
    //METODOS
    @Override
    public String toString(){
        return "-----=====Vendedor=====-----" + "\n" +
               "Usuario: " + this.getUsername() + ", " +
//               "Contraseña: " + this.getPassword() + ", " +
               "Apellidos: " + this.getApellido() + ", " +
               "Nombres: " + this.getNombre() + ", " + 
               "Id Interno: " + this.idInterno + 
               "\n";
    }
    
    @Override
    public ArrayList<Vehiculo> consultarStock(){
        ArrayList<Automovil> listAuto = SistemaConcesionaria.getListAutomovil();
        ArrayList<Motocicleta> listMoto = SistemaConcesionaria.getListMotocicleta();
        ArrayList<Camion> listCamion = SistemaConcesionaria.getListCamion();
        ArrayList<Tractor> listTractor = SistemaConcesionaria.getListTractor();
        ArrayList<String> vDisponible = new ArrayList();
        Automovil auto; Motocicleta moto; Camion camion; Tractor tractor;
        ArrayList<Vehiculo> listVehiculo = new ArrayList();
        
        for (int i = 0; i < listAuto.size(); i ++){
            auto = listAuto.get(i);
            if (!auto.getEstado().equals("Comprado") && !auto.getEstado().equals("Admitido")){
                listVehiculo.add(auto);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Automóvil --> "
                        + "Marca: " + auto.getMarca() + ", " + "Modelo: " 
                        + auto.getModelo() + ", " + "Año Fabricación: " 
                        + auto.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listMoto.size(); i ++){
            moto = listMoto.get(i);
            if (!moto.getEstado().equals("Comprado") && !moto.getEstado().equals("Admitido")){
                listVehiculo.add(moto);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Motocicleta --> "
                        + "Marca: " + moto.getMarca() + ", " + "Modelo: " 
                        + moto.getModelo() + ", " + "Año Fabricación: " 
                        + moto.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listCamion.size(); i ++){
            camion = listCamion.get(i);
            if (!camion.getEstado().equals("Comprado") && !camion.getEstado().equals("Admitido")){
                listVehiculo.add(camion);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Camión --> " 
                        + "Marca: " + camion.getMarca() + ", " + "Modelo: " 
                        + camion.getModelo() + ", " + "Año Fabricación: " 
                        + camion.getAñoFabricacion()
                        + "\n");
            }
        }
        for (int i = 0; i < listTractor.size(); i ++){
            tractor = listTractor.get(i);
            if (!tractor.getEstado().equals("Comprado") && !tractor.getEstado().equals("Admitido")){
                listVehiculo.add(tractor);
                int no = listVehiculo.size();
                vDisponible.add(no + ") " + "Tractor --> "
                        + "Marca: " + tractor.getMarca() + ", " + "Modelo: " 
                        + tractor.getModelo() + ", " + "Año Fabricación: " 
                        + tractor.getAñoFabricacion()
                        + "\n");
            }
        }
        System.out.println(vDisponible);
        return listVehiculo;
    }
    
//    public void solicitudesPendientes(){
//        for (int i = 0; i < solicitudes.size(); i++){
//            System.out.println(i + ") " + solicitudes.get(i));
//        }
//    }
    public void agregarSolicitud(ArrayList soli){
        this.solicitudCotizacion.add(soli);
    }
    public void removerSolicitud(int id){
        this.solicitudCotizacion.remove(id);
    }
    
    public static void procesoVendedor(String opc, Vendedor vendedor){
        boolean valopc = true;
        while(valopc){
            if (Main.validarOpcion(opc)){
                if (opc.equals("1")){
                    valopc = false;
                    Vendedor v = new Vendedor();
                    ArrayList<Vehiculo> listVehiculo = v.consultarStock();
                    if (listVehiculo.size() != 0){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        String opcAux;
                        opcAux = textoIngresado.nextLine();
                        while(!opcAux.equals("1")){
                            System.out.print("Por favor, digite correctamente el número para regresar: ");
                            opcAux = textoIngresado.nextLine();
                        }
                        Main.menuVendedor();
                    }else{
                        System.out.println("Actualmente el stock de vehículos " 
                                + "se encuentra vacío. Por favor, vuelva en otra ocasión."
                        );
                        Main.menuVendedor();
                    }
                }
                else if (opc.equals("2")){
                    valopc = false;
                    String user = ""; Vehiculo vehi = new Vehiculo("", "", 0, "", "", 0, 0); 
                    Cliente client = new Cliente(); Object obj;
                    int solis = 0;
                    ArrayList<ArrayList> listSoli;
                    ArrayList<Object> solicitud;
                    listSoli = vendedor.getSolicitudCotizacion();
                    for (int j = 0; j < listSoli.size(); j++){
                        solis = j + 1;
                        solicitud = listSoli.get(j);
                        for (int i = 0; i < solicitud.size(); i++){
                            if (i == 0){
                                obj = solicitud.get(i);
                                client = (Cliente)obj;
                                user = client.getUsername();
                            }
                            if (i == 2){
                                obj = solicitud.get(i);
                                vehi = (Vehiculo)obj;
                            }
                        }
                        System.out.println("--------------------------------------------------" + "\n"
                        + "Solicitud Pendiente No. " + "(" + solis + "): " + "Usuario: " + user + "\n"
                        + "Vehículo Solicitado --> " + vehi);
                    }
                    if (solis == 0){
                        System.out.println("--------------------------------------------------" + "\n"
                        + "No hay Solicitudes Pendiente(s)" + "\n"
                        + "--------------------------------------------------");
                        Main.menuVendedor();
                    }
                    String opcAux;
                    boolean validar = true;
                    while(validar){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Atender Solicitud" + "\n" 
                        + "2.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        opcAux = textoIngresado.nextLine();
                        if (Main.validarOpcion(opcAux) == true){
                            if(opcAux.equals("1")){
                                validar = false;
                                String opcV;
                                boolean valopcAux1 = true;
                                while(valopcAux1){
                                    System.out.print("De la lista de solicitudes, digite el número del que desea atender: ");
                                    opcV = textoIngresado.nextLine();
                                    if (Main.validarOpcion(opcV) == true){
                                        int op = Integer.parseInt(opcV);
                                        if(op <= vendedor.getSolicitudCotizacion().size()){
                                            valopcAux1 = false;
                                            String opcAux2;
                                            boolean validar1 = true;
                                            while(validar1){
                                                System.out.print("--------------------------------------------------" + "\n"
                                                + "1.- Aceptar Solicitud" + "\n" 
                                                + "2.- Rechazar Solicitud" + "\n" 
                                                + "--------------------------------------------------" + "\n"
                                                + "Por favor, digite el número de la acción que desea realizar: ");
                                                opcAux2 = textoIngresado.nextLine();
                                                if (Main.validarOpcion(opcAux2)){
                                                    if(opcAux2.equals("1")){
                                                        validar1 = false;
                                                        vehi = (Vehiculo)vendedor.getSolicitudCotizacion().get(op - 1).get(2);
                                                        client.agregarSolicitud("Aceptada", "", vehi, "Cotizacion");
                                                        System.out.println("Solicitud Aceptada!!!");
                                                        int id = Integer.parseInt(opcV);
                                                        id = id - 1;
                                                        vendedor.removerSolicitud(id);
                                                        Main.menuVendedor();
                                                    }
                                                    else if(opcAux2.equals("2")){
                                                        validar1 = false;
                                                        String motivo;
                                                        System.out.print("Motivo: ");
                                                        motivo = textoIngresado.nextLine();
                                                        vehi = (Vehiculo)vendedor.getSolicitudCotizacion().get(op - 1).get(2);
                                                        client.agregarSolicitud("Rechazada", motivo, vehi, "Cotizacion");
                                                        System.out.println("Solicitud Rechazada!!!");   
                                                        int id = Integer.parseInt(opcV);
                                                        id = id - 1;
                                                        vendedor.removerSolicitud(id);
                                                        Main.menuVendedor();
                                                    }
                                                    else{
                                                        System.out.println("******************************************************************************" 
                                                        + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if(opcAux.equals("2")){
                                validar = false;
                                Main.menuVendedor();
                            }else{
                                System.out.println("******************************************************************************" 
                                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                            }
                        }
                    }
                }
                else if (opc.equals("3")){
                    valopc = false;
                    Main.procesoInicial();
                }
                else if (opc.equals("4")){
                    Proceso.grabarDatos();
                    System.exit(0);
                }
                else{
                    valopc = false;
                    Main.menuVendedor();
                }
            }else{
                valopc = false;
                System.out.println("******************************************************************************" 
                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                Main.menuVendedor();
            }
        }
    }
}
