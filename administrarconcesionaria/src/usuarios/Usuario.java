/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import sistema.SistemaConcesionaria;

/**
 *
 * @author Carlos
 */
public abstract class Usuario extends SistemaConcesionaria{
    //ATRIBUTOS
    private String username;
    private String password;
    private String apellido;
    private String nombre;
    
    //CONSTRUCTOR CON PARAMETROS
    public Usuario(String username, String password, String apellido, String nombre){
        this.username = username;
        this.password = password;
        this.apellido = apellido;
        this.nombre = nombre;
    }
    
    //GETTERS
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public String getApellido(){
        return this.apellido;
    }
    public String getNombre(){
        return this.nombre;
    }
    
    
    //SETTERS
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setApellido(String apellido){
        this.apellido = apellido;
    }
    public void setNombre(String nombre){
        this.nombre = nombre;
    }
    
}
