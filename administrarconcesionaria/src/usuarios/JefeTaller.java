/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.util.ArrayList;
import java.util.Scanner;
import vehiculos.*;
import menu.Main;
import sistema.*;
/**
 *
 * @author Carlos
 */
public class JefeTaller extends Usuario{
    static Scanner textoIngresado = new Scanner(System.in);
    //ATRIBUTOS
    private ArrayList<String> certTecnicas = new ArrayList();
    private ArrayList<ArrayList> vehiEntregado = new ArrayList();
    private ArrayList<ArrayList> soliVehiMantenimiento = new ArrayList();   
    
    
    //CONSTRUCTOR
    public JefeTaller(){
        super("", "", "", "");
    }
    public JefeTaller(String user, String pass, String apellido, String nombre, ArrayList<String> certTecnicas){
        super(user, pass, apellido, nombre);
        this.certTecnicas = certTecnicas;
    }
    
    
    //GETTERS
    public ArrayList<String> getCertTecnicas(){
        return this.certTecnicas;
    }
    public ArrayList<ArrayList> getVehiEntregado(){
        return this.vehiEntregado;
    }
    public ArrayList<ArrayList> getSoliVehiMantenimiento() {
        return this.soliVehiMantenimiento;
    }    
    public void agregarCertTecnicas(String certTecnicas){
        this.certTecnicas.add(certTecnicas);
    }
    
    //METODOS
    @Override
    //Presentar la informacion de jefe taller
    public String toString(){
        return "-----=====Jefe de Taller=====-----" + "\n" +
               "Usuario: " + this.getUsername() + ", " +
//               "Contraseña: " + this.getPassword() + ", " +
               "Apellidos: " + this.getApellido() + ", " +
               "Nombres: " + this.getNombre() + ", " + 
               "Certificacones Técnicas: " + this.certTecnicas + 
               "\n";
    }
    //Remueve un arrayList de la lista de solicitudes de vehiculos en mantenimiento
    public void removerSolicitudMantenimiento(int id){
        this.soliVehiMantenimiento.remove(id);
    }
    //Agregar un arrayList a otro ArrayList llamado vehiEntregado
    public void agregarVehiComprado(String estado, Cliente cliente, Vehiculo vehiculo){
        ArrayList<Object> list = new ArrayList();
        list.add(estado); list.add(vehiculo);list.add(cliente); 
        this.vehiEntregado.add(list);
    }
    
    //Agrega un arrayList a la lista de solicitudes de mantenimiento
    public void agregarSoliVehiMantenimiento(ArrayList soli){
        this.soliVehiMantenimiento.add(soli);
    }
    // Se encarga de realizar el proceso interno buscar en el arryaLis ListaCompras
    // y extraer de el , el cliente y el vehiculo para así poderle agregar al metodo
    // agregarVehiComprado que se encarga de aggregarlo a la lista de vehiEntregado
    // asi poder afirmar que es un vehiculo entregado, luego seteamos el estado del vehiculo
    // a Comprado para asi lograr que se eliminen a simple vista del vendedor y cliente
    
    public static void procesarEntrega(JefeTaller jefeTaller, ArrayList<ArrayList> listCompras, int opc){
        Cliente cliente = new Cliente(); 
        Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
        opc = opc - 1;
        ArrayList<Object> compra = listCompras.get(opc);  
        for (int i = 0; i < compra.size(); i++){
            if (i == 0){
                cliente = (Cliente)compra.get(i);
            }
            if (i == 1) {
                vehiculo = (Vehiculo) compra.get(i);
            }
        }
        jefeTaller.agregarVehiComprado("Entregado", cliente, vehiculo);
        vehiculo.setEstado("Comprado");
        int noVehiComp = cliente.getNoVehiculoComprado();
        noVehiComp = noVehiComp + 1;
        cliente.setNoVehiculoComprado(noVehiComp);
        System.out.println("El vehículo a sido entregado satisfactoriamente. Cliente: " + cliente.getUsername() + "\n" + "Vehículo: " + vehiculo);
    }
    // Se encarga de recibir la lista de solicitudes de vehiculos para el mantenimiento
    //recorre la lista y obtiene de cada sublista los datos de cliente, jefe taller, vehiculo
    // y el tipo de solicitud es decir, preventivo o emergencia, en tal caso al ser preventivo
    // se procesa el precio final el cual depende de la distancia que le envie el cliente
    //y se procede agregar a las listas de mantenimiento preventivo, agregar la solicitud en cliente como aceptado
    // con tipo de solicitud en "Mantenimiento" y por ultimo setea el estado del  vehiculo a "Admitido! al taller
    public static void procesoAprobarSoli(ArrayList<ArrayList> listSoli, int opc,int distancia){
        opc = opc - 1;
        ArrayList<Object> manteni = listSoli.get(opc);  
        Cliente cliente = new Cliente(); 
        JefeTaller jT = new JefeTaller();
        Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
        String tiposoli = "";
        for (int i = 0; i < manteni.size(); i++){
            if(i == 0){
                cliente = (Cliente)manteni.get(i);
            }
            if(i == 1){
                jT = (JefeTaller)manteni.get(i);
            }
            if(i == 2){
                vehiculo = (Vehiculo) manteni.get(i);
            }
            if(i == 3){
                tiposoli = (String)manteni.get(i);
            }
        }
        if (tiposoli.equals("Preventivo")){
            float precio = (float) (distancia * 0.10);
            Mantenimiento.agregarMantePreventivo(cliente, vehiculo, distancia, "EnReparacion", precio);
            cliente.agregarSolicitud("Aceptada", "", vehiculo, "Mantenimiento");
            vehiculo.setEstado("Admitido");
            System.out.println("Solicitud Aceptada!!!");
        }
        if (tiposoli.equals("Emergencia")){
            boolean val = true;
            float costo = 0;
            while(val){
                System.out.print("Ingresar el precio por cada Km recorrido con decimales(,): ");
                try{
                    costo = textoIngresado.nextFloat();
                    val = false;
                }catch(Exception ex){
                    System.out.print("Por favor, ingrese correctamente el precio: ");
                }
            }
            float precio = (float) (distancia*costo);            
            Mantenimiento.agregarManteEmergencia(cliente, vehiculo, distancia,  "EnReparacion", costo, precio);
            cliente.agregarSolicitud("Aceptada", "", vehiculo, "Mantenimiento");
            vehiculo.setEstado("Admitido");
            System.out.println("Solicitud Aceptada!!!");
        }
        SistemaConcesionaria.getListJefeTaller().get(0).removerSolicitudMantenimiento(opc);
    }
    // camvia los estados de prueba a finalizado
    public static void procesoCambiarEstado(ArrayList<ArrayList> list1, int opc, String progreso){   
        opc = opc - 1;
        ArrayList<Object> mant = list1.get(opc);
        for (int i = 0; i < mant.size(); i++){
            if (i == 3) {
                mant.set(i, progreso);
            }
        }
    }
    public static void procesoJefeTaller(String opc, JefeTaller jefeTaller){
        boolean valopc = true;
        while(valopc){
            if (Main.validarOpcion(opc)){
                if (opc.equals("1")){
                    valopc = false;
                    ArrayList<ArrayList> list = new ArrayList();
                    ArrayList<Cliente> listCliente;
                    listCliente = SistemaConcesionaria.getListCliente();
                    int idSoliCompra = 0;
                    ArrayList<ArrayList> listCompras = new ArrayList();
                    ArrayList<Object> compra = new ArrayList();
                    for (Cliente cliente : listCliente){
                        list = cliente.getSolicitud();
                        Object obj; String estado = ""; 
                        String tpSolicitud = "";
                        Vehiculo v = new Vehiculo("","",0,"","",0,0);
                        for (int i = 0; i < list.size(); i++){
                            ArrayList<Object> soli = new ArrayList();
                            soli = list.get(i);
                            for (int k = 0; k < soli.size(); k ++){
                                if (k == 0){
                                    obj = soli.get(k);
                                    estado = (String)obj;                               
                                }
                                if (k == 2){
                                    obj = soli.get(k);
                                    v = (Vehiculo)obj;
                                }
                                if (k == 3){
                                    obj = soli.get(k);
                                    tpSolicitud = (String)obj;
                                }
                            }
                            if (tpSolicitud.equals("Compra") && estado.equals("Aceptada") && v.getEstado().equals("Solicitado")){
                                idSoliCompra = idSoliCompra + 1;
                                compra.add(cliente); compra.add(v); 
                                listCompras.add(compra);
                                System.out.print("--------------------------------------------------" + "\n"
                                + "Compra" + "(" + idSoliCompra + ")" + "\n"       
                                + cliente
                                + v + "\n");
                            }
                        } 
                    }      
                    boolean valopcAux = true;
                    String opcAux;
                    if (idSoliCompra != 0){
                        while(valopcAux){
                            System.out.print("--------------------------------------------------" + "\n"
                            + "1.- Vehículo a entregar" + "\n" 
                            + "2.- Regresar" + "\n" 
                            + "--------------------------------------------------" + "\n"
                            + "Por favor, digite el número de la acción que desea realizar: ");   
                            opcAux = textoIngresado.nextLine();
                            if (Main.validarOpcion(opcAux) == true){
                                if (opcAux.equals("1")){
                                    valopcAux = false;
                                    String opcV;
                                    boolean valopcAux1 = true;
                                    while(valopcAux1){
                                        System.out.print("De la lista de compra, digite el número del que desea entregar: ");
                                        opcV = textoIngresado.nextLine();
                                        if (Main.validarOpcion(opcV) == true){
                                            int op = Integer.parseInt(opcV);
                                            if(op <= idSoliCompra){
                                                valopcAux1 = false;
                                                procesarEntrega(jefeTaller, listCompras, op);
                                                Main.menuJefeTaller();
                                            }
                                        }
                                    }
                                }
                                if (opcAux.equals("2")){
                                    valopcAux = false;
                                    Main.menuJefeTaller();
                                }
                            }
                        }
                    }else{
                        System.out.println("--------------------------------------------------" + "\n"
                        + "No hay vehículo(s) por entregar." + "\n"
                        + "--------------------------------------------------");
                        Main.menuJefeTaller();
                    }
                }
                else if(opc.equals("2")){
                    valopc = false;
                    ArrayList<ArrayList> listSoli;
                    listSoli = jefeTaller.getSoliVehiMantenimiento();
                    if (listSoli==null || listSoli.size()== 0) {
                        System.out.println("--------------------------------------------------" + "\n"
                        + "No hay solicitud(s) para presentar." + "\n"
                        + "--------------------------------------------------");
                        Main.menuJefeTaller();
                    }else{
                        int idSoli = 0;
                        ArrayList<Object> soli = new ArrayList();
                        int distancia = 0;
                        for (ArrayList<Object> subListSoli : listSoli){
                            Object obj;
                            String estado="";
                            Vehiculo v = new Vehiculo("","",0,"","",0,0);
                            Cliente clie = new Cliente();
                            
                            for (int i = 0; i < subListSoli.size(); i++){
                                if (i == 3){
                                    obj = subListSoli.get(i);
                                    estado = (String)obj;
                                }
                                if (i == 0){
                                    obj = subListSoli.get(i);
                                    ArrayList<ArrayList> list = new ArrayList();
                                    clie = (Cliente)obj;
                                }
                                if (i == 2){
                                    obj = subListSoli.get(i);
                                    v = (Vehiculo)obj;
                                }
                                if (i == 4){
                                    obj = subListSoli.get(i);
                                    distancia = (int)obj;
                                }
                            }
                            if (estado.equals("Preventivo")||estado.equals("Emergencia") ){
                                    idSoli = idSoli + 1;
                                    System.out.print("--------------------------------------------------" + "\n"
                                    + "Solicitud Mantenimiento" + "(" + idSoli + ")" + "\n" +"Cliente: "    
                                    + clie + "Vehiculo: "
                                    + v + "Estado: " + estado + "\n" + "Distancia [km(s)]: " + distancia + "\n"
                                    + "--------------------------------------------------");
                            }
                        } 
                        boolean valopcAux = true;
                        String opcAux;
                        if (idSoli != 0){
                            while(valopcAux){
                                System.out.print("--------------------------------------------------" + "\n"
                                + "1.- Aprobar Solicitud" + "\n" 
                                + "2.- Regresar" + "\n" 
                                + "--------------------------------------------------" + "\n"
                                + "Por favor, digite el número de la acción que desea realizar: ");   
                                opcAux = textoIngresado.nextLine();
                                if (Main.validarOpcion(opcAux) == true){
                                    if (opcAux.equals("1")){
                                        valopcAux = false;
                                        String opcV;
                                        boolean valopcAux1 = true;
                                        while(valopcAux1){
                                            System.out.print("De la lista de solicitudes, digite el número del que desea aprobar: ");
                                            opcV = textoIngresado.nextLine();
                                            if (Main.validarOpcion(opcV) == true){
                                                int op = Integer.parseInt(opcV);
    //                                            System.out.println(op + " " + idSoliCompra);
                                                if(op <= idSoli){
                                                    valopcAux1 = false;
//                                                    System.out.print("Para aprobar y confirmar el precio final es necesario que ingrese la distancia");
//                                                    int distancia = textoIngresado.nextInt();
                                                    procesoAprobarSoli(listSoli, op, distancia);
                                                    Main.menuJefeTaller();
                                                }
                                            }
                                        }
                                    }
                                    if (opcAux.equals("2")){
                                        valopcAux = false;
                                        Main.menuJefeTaller();
                                    }
                                }
                            }
                        }else{
                            System.out.println("--------------------------------------------------" + "\n"
                            + "No hay solicitud(s) por presentar." + "\n"
                            + "--------------------------------------------------");
                            Main.menuJefeTaller();
                        }    
                    }
                }
                else if (opc.equals("3")){
                    valopc=false;
                    ArrayList<ArrayList> list1 = Mantenimiento.getListMantenimientoPreventivo();
                    ArrayList<ArrayList> list2 = Mantenimiento.getListMantenimientoEmergencia();
                    Vehiculo v = new Vehiculo("","",0,"","",0,0);
                    if ((list1 == null || list1.size()== 0) && (list2==null || list2.size()== 0))  {
                        System.out.println("--------------------------------------------------" + "\n"
                        + "No hay vehículo(s) en mantenimieno." + "\n"
                        + "--------------------------------------------------");
                        Main.menuJefeTaller();
                    }else{
                        String progreso=""; int distancia = 0;
                        Cliente clie = new Cliente();
                        int noEPreventivo = 0;
                        for (ArrayList<Object> obj : list1){
                            for (int i = 0; i < obj.size(); i++){
                                if (i == 0){
                                    clie = (Cliente)obj.get(i);;
                                }
                                if (i == 1){
                                    v = (Vehiculo)obj.get(i);
                                }
                                if (i == 2){
                                    distancia = (int)obj.get(i);
                                }
                                if (i == 3){
                                    progreso = (String)obj.get(i);
                                }  
                            }
                            if (!progreso.equals("Finalizado")){
                                noEPreventivo = noEPreventivo + 1;
                                    System.out.print("--------------------------------------------------" + "\n"
                                + "No. Mantenimiento" + "(" + noEPreventivo + ")" +" del Cliente:" +    
                                clie + "Vehículo: " + v
                                + "Progreso: " + progreso + "\n");
                            }
                        }
                        int noEEmergencia = noEPreventivo;
                        for (ArrayList<Object> obj : list2){
                            for (int i = 0; i < obj.size(); i++){
                                if (i == 0){
                                    clie = (Cliente)obj.get(i);;
                                }
                                if (i == 1){
                                    v = (Vehiculo)obj.get(i);
                                }
                                if (i == 2){
                                    distancia = (int)obj.get(i);
                                }
                                if (i == 3){
                                    progreso = (String)obj.get(i);
                                }  
                            }
                            if (!progreso.equals("Finalizado")){
                                noEEmergencia = noEEmergencia + 1;
                                System.out.print("--------------------------------------------------" + "\n"
                                    + "No. Mantenimiento" + "(" + noEEmergencia + ")" + " del Cliente:" +    
                                    clie + "Vehículo: "
                                    + v + "Progreso: " + progreso + "\n");
                            }
                        }
                        boolean valopcAux = true;
                        String opcAux;
                        if (noEEmergencia != 0){
                            while(valopcAux){
                                System.out.print("--------------------------------------------------" + "\n"
                                + "1.- Cambiar estado del progreso " + "\n" 
                                + "2.- Regresar" + "\n" 
                                + "--------------------------------------------------" + "\n"
                                + "Por favor, digite el número de la acción que desea realizar: ");   
                                opcAux = textoIngresado.nextLine();
                                if (Main.validarOpcion(opcAux) == true){
                                    if (opcAux.equals("1")){
                                        valopcAux = false;
                                        boolean valopcAux1 = true;
                                        while(valopcAux1){
                                            System.out.print("De la lista de Mantenimientos, digite el número del que desea cambiar: ");
                                            String opcV = textoIngresado.nextLine();
                                            if (Main.validarOpcion(opcV) == true){                                            
                                                int op = Integer.parseInt(opcV);
                                                if(op <= noEEmergencia){
                                                    valopcAux1 = false;
                                                    boolean valopcAux2 = true;                                                
                                                    while(valopcAux2){
                                                        System.out.print("--------------------------------------------------" + "\n"
                                                        + "1.- Cambiar a etapa de prueba " + "\n" 
                                                        + "2.- Cambiar a Mantenimiento Finalizado" + "\n" 
                                                        + "--------------------------------------------------" + "\n"
                                                        + "Por favor, digite el número de la acción que desea realizar: ");   
                                                        String opcAux2 = textoIngresado.nextLine();
                                                        if (Main.validarOpcion(opcAux2) == true){
                                                            if (opcAux2.equals("1")){
                                                                valopcAux2 = false;
                                                                if (op <= noEPreventivo){
                                                                    procesoCambiarEstado(list1, op, "EnEtapaDePrueba");
                                                                    System.out.println(
                                                                              "---------------------------------------------------------" + "\n"
                                                                            + "Progreso del mantenimiento cambiado satisfactoriamente!!!" + "\n"
                                                                            + "---------------------------------------------------------");
                                                                    Main.menuJefeTaller();
                                                                }else{       
                                                                    int valAux = noEEmergencia - noEPreventivo;
                                                                    procesoCambiarEstado(list2, valAux, "EnEtapaDePrueba");
                                                                    System.out.println(
                                                                              "---------------------------------------------------------" + "\n"
                                                                            + "Progreso del mantenimiento cambiado satisfactoriamente!!!" + "\n"
                                                                            + "---------------------------------------------------------");
                                                                    Main.menuJefeTaller();
                                                                }
                                                            }
                                                            if (opcAux2.equals("2")){
                                                                valopcAux2 = false;
                                                                if (op <= noEPreventivo){
                                                                    procesoCambiarEstado(list1, op, "Finalizado");
                                                                    v.setEstado("Comprado");
                                                                    System.out.println(
                                                                              "---------------------------------------------------------" + "\n"
                                                                            + "Progreso del mantenimiento cambiado satisfactoriamente!!!" + "\n"
                                                                            + "---------------------------------------------------------");
                                                                    Main.menuJefeTaller();
                                                                }else{
                                                                    int valAux = noEEmergencia - noEPreventivo;
                                                                    procesoCambiarEstado(list2, valAux, "Finalizado");
                                                                    v.setEstado("Comprado");
                                                                    System.out.println(
                                                                              "---------------------------------------------------------" + "\n"
                                                                            + "Progreso del mantenimiento cambiado satisfactoriamente!!!" + "\n"
                                                                            + "---------------------------------------------------------");
                                                                    Main.menuJefeTaller();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (opcAux.equals("2")){
                                        valopcAux = false;
                                        Main.menuJefeTaller();
                                    }
                                }
                            }
                        }else{
                            System.out.println("--------------------------------------------------" + "\n"
                            + "No hay vehículo(s) en mantenimieno." + "\n"
                            + "--------------------------------------------------");
                            Main.menuJefeTaller();
                        }
                    }  
                }
                else if (opc.equals("4")){
                    valopc = false;
                    Main.procesoInicial();
                }
                else if (opc.equals("5")){
                    Proceso.grabarDatos();
                    System.exit(0);
                }
                else{
                    valopc = false;
                    Main.menuJefeTaller();
                }
            }else{
                System.out.println("******************************************************************************" 
                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                Main.menuJefeTaller();
            }
            
        }
    }
}