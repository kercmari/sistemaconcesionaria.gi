/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuarios;

import java.util.ArrayList;
import java.util.Scanner;
import vehiculos.*;
import menu.Main;
import sistema.*;

/**
 *
 * @author Carlos
 */
public class Supervisor extends Usuario{
    static Scanner textoIngresado = new Scanner(System.in);
    //ATRIBUTOS
    private ArrayList<String> certAcademicas = new ArrayList();
    private ArrayList<ArrayList> solicitudCompra = new ArrayList();
    
    //CONSTRUCTOR
    public Supervisor(){
        super("", "", "", "");
    }
    public Supervisor(String user, String pass, String apellido, String nombre, ArrayList<String> certAcademicas){
        super(user, pass, apellido, nombre);
        this.certAcademicas = certAcademicas;
    }
    
    
    //GETTERS
    public ArrayList<String> getCertAcademicas(){
        return this.certAcademicas;
    }
    
    public void agregarCertAcademicas(String certAcademicas){
        this.certAcademicas.add(certAcademicas);
    }
    public ArrayList<ArrayList> getSolicitudCompra(){
        return this.solicitudCompra;
    }
    
    //METODOS
    @Override
    public String toString(){
        return "-----=====Supervisor=====-----" + "\n" +
               "Usuario: " + this.getUsername() + ", " +
//               "Contraseña: " + this.getPassword() + ", " +
               "Apellidos: " + this.getApellido() + ", " +
               "Nombres: " + this.getNombre() + ", " + 
               "Certificaciones Académicas: " + this.certAcademicas + 
               "\n";
    }
    
    public void agregarSolicitud(ArrayList soli){
        this.solicitudCompra.add(soli);
//        System.out.println(solicitudCompra);
    }
    public void removerSolicitud(int id){
        this.solicitudCompra.remove(id);
    }
    
    public static void procesoSupervisor(String opc, Supervisor supervisor){
        boolean valopc = true;
        while(valopc){
            if (Main.validarOpcion(opc)){
                if (opc.equals("1")){
                    valopc = false;
                    String user = ""; Vehiculo vehi = new Vehiculo("", "", 0, "", "", 0, 0); 
                    Cliente client = new Cliente(); Object obj;
                    int solis = 0;
                    ArrayList<ArrayList> listSoli;
                    ArrayList<Object> solicitud;
                    listSoli = supervisor.getSolicitudCompra();
                    for (int j = 0; j < listSoli.size(); j++){
                        solis = j + 1;
                        solicitud = listSoli.get(j);
                        for (int i = 0; i < solicitud.size(); i++){
                            if (i == 0){
                                obj = solicitud.get(i);
                                client = (Cliente)obj;
                                user = client.getUsername();
                            }
                            if (i == 2){
                                obj = solicitud.get(i);
                                vehi = (Vehiculo)obj;
                            }
                        }
                        System.out.println("--------------------------------------------------" + "\n"
                        + "Solicitud Pendiente No. " + "(" + solis + "): " + "Usuario: " + user + "\n"
                        + "Vehículo Solicitado --> " + vehi);
                    }
                    if (solis == 0){
                        System.out.println("--------------------------------------------------" + "\n"
                        + "No hay Solicitudes Pendiente(s)" + "\n"
                        + "--------------------------------------------------");
                        Main.menuSupervisor();
                    }
                    String opcAux;
                    boolean validar = true;
                    while(validar){
                        System.out.print("--------------------------------------------------" + "\n"
                        + "1.- Atender Solicitud" + "\n" 
                        + "2.- Regresar" + "\n" 
                        + "--------------------------------------------------" + "\n"
                        + "Por favor, digite el número de la acción que desea realizar: ");
                        opcAux = textoIngresado.nextLine();
                        if (Main.validarOpcion(opcAux) == true){
                            if(opcAux.equals("1")){
                                validar = false;
                                String opcV;
                                boolean valopcAux1 = true;
                                while(valopcAux1){
                                    System.out.print("De la lista de solicitudes, digite el número del que desea atender: ");
                                    opcV = textoIngresado.nextLine();
                                    if (Main.validarOpcion(opcV) == true){
                                        int op = Integer.parseInt(opcV);
                                        if(op <= supervisor.getSolicitudCompra().size()){
                                            valopcAux1 = false;
                                            String opcAux2;
                                            boolean validar1 = true;
                                            while(validar1){
                                                System.out.print("--------------------------------------------------" + "\n"
                                                + "1.- Aceptar Solicitud" + "\n" 
                                                + "2.- Rechazar Solicitud" + "\n" 
                                                + "--------------------------------------------------" + "\n"
                                                + "Por favor, digite el número de la acción que desea realizar: ");
                                                opcAux2 = textoIngresado.nextLine();
                                                if (Main.validarOpcion(opcAux2)){
                                                    if(opcAux2.equals("1")){
                                                        validar1 = false;
                                                        vehi = (Vehiculo)supervisor.getSolicitudCompra().get(op - 1).get(2);
                                                        client.agregarSolicitud("Aceptada", "", vehi, "Compra");
                                                        System.out.println("Solicitud Aceptada!!!");
                                                        int id = Integer.parseInt(opcV);
                                                        id = id - 1;
                                                        supervisor.removerSolicitud(id);
                                                        Main.menuSupervisor();
                                                    }
                                                    else if(opcAux2.equals("2")){
                                                        validar1 = false;
                                                        String motivo;
                                                        System.out.print("Motivo: ");
                                                        motivo = textoIngresado.nextLine();
                                                        vehi = (Vehiculo)supervisor.getSolicitudCompra().get(op - 1).get(2);
                                                        vehi.setEstado("Disponible");
                                                        client.agregarSolicitud("Rechazada", motivo, vehi, "Compra");
                                                        System.out.println("Solicitud Rechazada!!!");   
                                                        int id = Integer.parseInt(opcV);
                                                        id = id - 1;
                                                        supervisor.removerSolicitud(id);
                                                        Main.menuSupervisor();
                                                    }
                                                    else{
                                                        System.out.println("******************************************************************************" 
                                                        + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if(opcAux.equals("2")){
                                validar = false;
                                Main.menuSupervisor();
                            }else{
                                System.out.println("******************************************************************************" 
                                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                            }
                        }
                    }
                }
                else if (opc.equals("2")){
                    valopc = false;
                    Main.procesoInicial();
                }
                else if (opc.equals("3")){
                    Proceso.grabarDatos();
                    System.exit(0);
                }
                else{
                    valopc = false;
                    Main.menuSupervisor();
                }
            }else{
                System.out.println("******************************************************************************" 
                + "\n" + "Por favor, digite correctamente el número de la acción que desea realizar!");
                Main.menuSupervisor();
//                valopc = false;
            }
        }
    }
}
