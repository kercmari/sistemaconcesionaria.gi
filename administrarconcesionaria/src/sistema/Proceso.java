/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;

import java.io.BufferedReader;
import java.io.File;
import usuarios.*;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;
import vehiculos.*;


/**
 *
 * @author Carlos
 */
public class Proceso{
    
    
    private static String rutaBaseDatosCliente = "cliente.txt";
    private static String rutaBaseDatosVendedor = "vendedor.txt";
    private static String rutaBaseDatosSupervisor = "supervisor.txt";
    private static String rutaBaseDatosJefeTaller = "jefeTaller.txt";
    private static String rutaBaseDatosAutomovil = "automovil.txt";
    private static String rutaBaseDatosMotocicleta = "motocicleta.txt";
    private static String rutaBaseDatosCamion = "camion.txt";
    private static String rutaBaseDatosTractor = "tractor.txt";
    private static String rutaBaseDatosSolicitudesCliente = "solicliente.txt";
    private static String rutaBaseDatosSolicitudesVendedor = "solivendedor.txt";
    private static String rutaBaseDatosSolicitudesSupervisor = "solisupervisor.txt";
    private static String rutaBaseDatosMantenimientoPreventivo = "mantpreventivo.txt";
    private static String rutaBaseDatosMantenimientoEmergencia = "mantemergencia.txt";
    private static String rutaBaseDatosVehiEntregado = "vehientregado.txt";
    private static String rutaBaseDatosSoliVehiMantenimiento = "solivehimantenimiento.txt";
    
//    Proceso rp;
//    public static void main(String[] args) throws IOException{
//        cargarCliente();   
//    }
    //ESTA CLASE HACE TODO EL PROCESO DE MANEJO DE INFORMACION 
    //ES LA ENCARGADA DE CARGAR TODA LA INFORMACION GUARDADA AL MOMENTO DE INICIAR EL PROGRAMA AL IGUAL QUE ES LA 
    //ENCARGA DE GRABAR TODA LA INFORMACION QUE SE MANEJA EN EL SISTEMA AL MOMENTO DE DARLE A SALIR DEL SISTEMA
    public Proceso(){
        //CARGAMOS TODA LA INFORMACION AL MOMENTO DE CREAR EL PROCESO
        cargarCliente();
        cargarVendedor();
        cargarSupervisor();
        cargarJefeTaller();
        cargarAutomovil();
        cargarMotocicleta();
        cargarCamion();
        cargarTractor();
        cargarSolicitudesCliente();
        cargarSolicitudesVendedor();
        cargarSolicitudesSupervisor();
        cargarVehiEntregado();
        cargarSoliVehiMantenimiento();
        cargarMantenimientoPreventivo();
        cargarMantenimientoEmergencia();
    }
    //TODOS LOS METODOS DE GRABAR Y CARGAR SON EXACTAMENTE IGUALES CADA UNO HACE EL PROCESO DE GUARDAR LA INFORMACION
    //DE SUS RESPECTIVAS LISTAS Y DE CARGAR LA INFORMACION DE SUS RESPECTIVAS LISTAS
    //ES POR ELLO
    //QUE CADA UNO TIENE SU PROPIO ARCHIVO DE TEXTO QUE ES EN DONDE SE GUARDA TODA LA INFORMACION DENTRO DEL COMPUTADOR
    //Y DE IGUAL FORMA POR ELLO ES NECESARIO UN PROCESO PARA CARGAR CADA UNO DE ELLOS
    
    //METODOS PARA GRABAR INFORMACION
    public static void grabarCliente(ArrayList<Cliente> clientes){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosCliente);
            pw = new PrintWriter(fw);
            for (Cliente cli : clientes){
                pw.println(String.valueOf(cli.getUsername() + ", " 
                        + cli.getPassword() + ", " 
                        + cli.getApellido() + ", "  
                        + cli.getNombre() + ", " 
                        + cli.getNoVehiculoComprado() + ", " 
                        + cli.getCedula() + ", " 
                        + cli.getOcupacion() + ", " 
                        + cli.getIngresosMensuales()
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarVendedor(ArrayList<Vendedor> vendedores){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosVendedor);
            pw = new PrintWriter(fw);
            for (Vendedor ven : vendedores){
                pw.println(String.valueOf(ven.getUsername() + ", " 
                        + ven.getPassword() + ", " 
                        + ven.getApellido() + ", "  
                        + ven.getNombre() + ", " 
                        + ven.getIdInterno()
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarSupervisor(ArrayList<Supervisor> supervisores){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosSupervisor);
            pw = new PrintWriter(fw);
            for (Supervisor sup : supervisores){
                pw.print(String.valueOf(sup.getUsername() + ", " 
                    + sup.getPassword() + ", " 
                    + sup.getApellido() + ", "  
                    + sup.getNombre()));

                for (String certAca : sup.getCertAcademicas()){
                    pw.print(String.valueOf(", " + certAca));
                }
                pw.print(String.valueOf("\n"));    
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarJefeTaller(ArrayList<JefeTaller> jefeTaller){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosJefeTaller);
            pw = new PrintWriter(fw);
            for (JefeTaller jt : jefeTaller){
                pw.print(String.valueOf(jt.getUsername() + ", " 
                    + jt.getPassword() + ", " 
                    + jt.getApellido() + ", "  
                    + jt.getNombre()));

                for (String certTec : jt.getCertTecnicas()){
                    pw.print(String.valueOf(", " + certTec));
                }
                pw.print(String.valueOf("\n"));    
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarAutomovil(ArrayList<Automovil> automovil){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosAutomovil);
            pw = new PrintWriter(fw);
            for (Automovil auto : automovil){
                pw.println(String.valueOf(auto.getMarca() + ", " 
                        + auto.getModelo() + ", " 
                        + auto.getAñoFabricacion() + ", "  
                        + auto.getEstado() + ", " 
                        + auto.getMotor() + ", " 
                        + auto.getNoLLantas() + ", " 
                        + auto.getPrecio() + ", " 
                        + auto.getNoAsiento() + ", " 
                        + auto.getConvertible() + ", " 
                        + auto.getCamaraRetro()
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarMotocicleta(ArrayList<Motocicleta> motocicleta){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosMotocicleta);
            pw = new PrintWriter(fw);
            for (Motocicleta moto : motocicleta){
                pw.println(String.valueOf(moto.getMarca() + ", " 
                        + moto.getModelo() + ", " 
                        + moto.getAñoFabricacion() + ", "  
                        + moto.getEstado() + ", " 
                        + moto.getMotor() + ", " 
                        + moto.getNoLLantas() + ", " 
                        + moto.getPrecio() + ", " 
                        + moto.getCategoria() 
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarCamion(ArrayList<Camion> camion){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosCamion);
            pw = new PrintWriter(fw);
            for (Camion truck : camion){
                pw.println(String.valueOf(truck.getMarca() + ", " 
                        + truck.getModelo() + ", " 
                        + truck.getAñoFabricacion() + ", "  
                        + truck.getEstado() + ", " 
                        + truck.getMotor() + ", " 
                        + truck.getNoLLantas() + ", " 
                        + truck.getPrecio() + ", " 
                        + truck.getCapacidadCarga() + ", " 
                        + truck.getNoEje()
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarTractor(ArrayList<Tractor> tractor){
        try{
            //
            //CREA UN ARCHIVO Y PARA ELLO SE NECESITO UN TRY CATCH QUE SE ENCARGARA
            //DE CONTROLAR UNA POSIBLE EXCEPCION COMO POR EJEMPLO QUE LA RUTA QUE SE LE MANDO
            //COMO DIRECCION DE NUESTRO ARCHIVO EN DONDE GUARDAREMOS LA INFORMACION NO LA ENCUENTRE
            //AHI EL TRY CATCH ACTUARA CONTROLANDO LA EXCEPCION MOSTRANDO POR PANTALLA EL ERROR PERO 
            //SIN HACER QUE SE NOS CAIGA NUESTRO SISTEMA
            //LE ASIGNA LA RUTA DE NUESTRO ARCHIVO DE TEXTO CORRESPONDIENTE
            //RECORRE LA LISTA RESPECTIVA DE LA INFORMACION QUE QUEREMOS GUARDAR
            //COMIENZA A GUARDAR CADA DATO COMO TOKEN SEPARANDOLO POR UN ", "
            //PARA ASI SEPARAR LA INFORMACION
            //POR ULTIMO CIERRA EL ARCHIVO
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosTractor);
            pw = new PrintWriter(fw);
            for (Tractor trac : tractor){
                pw.println(String.valueOf(trac.getMarca() + ", " 
                        + trac.getModelo() + ", " 
                        + trac.getAñoFabricacion() + ", "  
                        + trac.getEstado() + ", " 
                        + trac.getMotor() + ", " 
                        + trac.getNoLLantas() + ", " 
                        + trac.getPrecio() + ", " 
                        + trac.getUsoAgricola() + ", " 
                        + trac.getTipoTransmision()
                ));
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    
    //METODOS PARA CARGAR INFORMACION
    private static void cargarCliente(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente client;
            File ruta = new File(rutaBaseDatosCliente);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            String linea = null;
            while((linea = bu.readLine()) != null){
                StringTokenizer st = new StringTokenizer(linea, ", ");
                client = new Cliente();

                String user = st.nextToken();
                client.setUsername(user);
                String pass = st.nextToken();
                client.setPassword(pass);

                client.setApellido(st.nextToken());
                client.setNombre(st.nextToken());
                client.setNoVehiculoComprado(Integer.parseInt(st.nextToken()));
                client.setCedula(st.nextToken());
                client.setOcupacion(st.nextToken());
                client.setIngresosMensuales(Float.parseFloat(st.nextToken()));

                SistemaConcesionaria.agregarListaCliente(client);
                SistemaConcesionaria.agregarUsuario("c" + "," + user + "," + pass);

//                System.out.println(SistemaConcesionaria.getListCliente()); 
            }
            bu.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarVendedor(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Vendedor vendedor;
            File ruta = new File(rutaBaseDatosVendedor);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            String seller = null;
            while((seller = bu.readLine()) != null){
                StringTokenizer st = new StringTokenizer(seller, ", ");
                vendedor = new Vendedor();

                String user = st.nextToken();
                vendedor.setUsername(user);
                String pass = st.nextToken();
                vendedor.setPassword(pass);

                vendedor.setApellido(st.nextToken());
                vendedor.setNombre(st.nextToken());
                vendedor.setIdInterno(Integer.parseInt(st.nextToken()));


                SistemaConcesionaria.agregarListaVendedor(vendedor);
                SistemaConcesionaria.agregarUsuario("v" + "," + user + "," + pass);

//                System.out.println(SistemaConcesionaria.getListVendedor()); 
            }
            bu.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarSupervisor(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Supervisor supervisor;
            File ruta = new File(rutaBaseDatosSupervisor);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            String supervis = null;
            while((supervis = bu.readLine()) != null){
                StringTokenizer st = new StringTokenizer(supervis, ", ");
                supervisor = new Supervisor();

                String user = st.nextToken();
                supervisor.setUsername(user);
                String pass = st.nextToken();
                supervisor.setPassword(pass);

                supervisor.setApellido(st.nextToken());
                supervisor.setNombre(st.nextToken());

                int toke = st.countTokens();
                for (int i = 0; i < toke; i++){
                    supervisor.agregarCertAcademicas(st.nextToken());
                }


                SistemaConcesionaria.agregarListaSupervisor(supervisor);
                SistemaConcesionaria.agregarUsuario("s" + "," + user + "," + pass);

//                System.out.println(SistemaConcesionaria.getListSupervisor()); 
            }
            bu.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarJefeTaller(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            JefeTaller jefeTaller;
            File ruta = new File(rutaBaseDatosJefeTaller);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            String boss = null;
            while((boss = bu.readLine()) != null){
                StringTokenizer st = new StringTokenizer(boss, ", ");
                jefeTaller = new JefeTaller();

                String user = st.nextToken();
                jefeTaller.setUsername(user);
                String pass = st.nextToken();
                jefeTaller.setPassword(pass);

                jefeTaller.setApellido(st.nextToken());
                jefeTaller.setNombre(st.nextToken());
    //          
                int toke = st.countTokens();
                for (int i = 0; i < toke; i++){
                    jefeTaller.agregarCertTecnicas(st.nextToken());
                }

                SistemaConcesionaria.agregarListaJefeTaller(jefeTaller);
                SistemaConcesionaria.agregarUsuario("j" + "," + user + "," + pass);

//                System.out.println(SistemaConcesionaria.getListJefeTaller()); 
            }
            bu.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    private static void cargarAutomovil(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Automovil auto;
            File ruta = new File(rutaBaseDatosAutomovil);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String automovil = null;
            while((automovil = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(automovil, ", ");
                auto = new Automovil();

                auto.setMarca(st.nextToken());
                auto.setModelo(st.nextToken());
                auto.setAñoFabricacion(Integer.parseInt(st.nextToken()));
                auto.setEstado(st.nextToken());
                auto.setMotor(st.nextToken());
                auto.setNoLLantas(Integer.parseInt(st.nextToken()));
                auto.setPrecio(Float.parseFloat(st.nextToken()));
                auto.setNoAsiento(Integer.parseInt(st.nextToken()));
                auto.setConvertible(Boolean.parseBoolean(st.nextToken()));
                auto.setCamaraRetro(Boolean.parseBoolean(st.nextToken()));


                SistemaConcesionaria.agregarAutomovil(auto);

//                System.out.println(SistemaConcesionaria.getListAutomovil()); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarMotocicleta(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Motocicleta moto;
            File ruta = new File(rutaBaseDatosMotocicleta);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String motocicleta = null;
            while((motocicleta = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(motocicleta, ", ");
                moto = new Motocicleta();

                moto.setMarca(st.nextToken());
                moto.setModelo(st.nextToken());
                moto.setAñoFabricacion(Integer.parseInt(st.nextToken()));
                moto.setEstado(st.nextToken());
                moto.setMotor(st.nextToken());
                moto.setNoLLantas(Integer.parseInt(st.nextToken()));
                moto.setPrecio(Float.parseFloat(st.nextToken()));
                moto.setCategoria(st.nextToken());

                SistemaConcesionaria.agregarMotocicleta(moto);

//                System.out.println(SistemaConcesionaria.getListMotocicleta()); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarCamion(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Camion camion;
            File ruta = new File(rutaBaseDatosCamion);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String truck = null;
            while((truck = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(truck, ", ");
                camion = new Camion();

                camion.setMarca(st.nextToken());
                camion.setModelo(st.nextToken());
                camion.setAñoFabricacion(Integer.parseInt(st.nextToken()));
                camion.setEstado(st.nextToken());
                camion.setMotor(st.nextToken());
                camion.setNoLLantas(Integer.parseInt(st.nextToken()));
                camion.setPrecio(Float.parseFloat(st.nextToken()));
                camion.setCapacidadCarga(Float.parseFloat(st.nextToken()));
                camion.setNoEje(Integer.parseInt(st.nextToken()));

                SistemaConcesionaria.agregarCamion(camion);

//                System.out.println(SistemaConcesionaria.getListCamion()); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    private static void cargarTractor(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Tractor trac;
            File ruta = new File(rutaBaseDatosTractor);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String tractor = null;
            while((tractor = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(tractor, ", ");
                trac = new Tractor();

                trac.setMarca(st.nextToken());
                trac.setModelo(st.nextToken());
                trac.setAñoFabricacion(Integer.parseInt(st.nextToken()));
                trac.setEstado(st.nextToken());
                trac.setMotor(st.nextToken());
                trac.setNoLLantas(Integer.parseInt(st.nextToken()));
                trac.setPrecio(Float.parseFloat(st.nextToken()));
                trac.setUsoAgricola(Boolean.parseBoolean(st.nextToken()));
                trac.setTipoTransmision(st.nextToken());

                SistemaConcesionaria.agregarTractor(trac);

//                System.out.println(SistemaConcesionaria.getListTractor()); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    
    private static void cargarSolicitudesVendedor(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente(); Vendedor vendedor = new Vendedor();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosSolicitudesVendedor);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                String user2 = st.nextToken();
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                    ArrayList<ArrayList> listVehiculos = new ArrayList();
                    ArrayList<Object> vehicle;
                    listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                    listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                    listVehiculos.add(SistemaConcesionaria.getListCamion());
                    listVehiculos.add(SistemaConcesionaria.getListTractor());
                    
                    
                    for (int j = 0; j < listVehiculos.size(); j++){
                        vehicle = listVehiculos.get(j);
                        Vehiculo v;
                        for (int i = 0; i < vehicle.size(); i++){
                            v = (Vehiculo)vehicle.get(i);
                            if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                                v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                                v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                                v.getPrecio() == precio)
                            {
                                vehiculo = v;
                            }
                        }
                    }
                
                int a = 0;
                for (int i = 0; i < SistemaConcesionaria.getListVendedor().size(); i++){
                    String usuario = SistemaConcesionaria.getListVendedor().get(i).getUsername();
                    if (usuario.equals(user2)){
                        vendedor = SistemaConcesionaria.getListVendedor().get(i);
                        a = 1;
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        if (a == 1){
                            cliente.solicitarCotizacion(cliente, vendedor, vehiculo);
                        }
                    }
                }
                cliente = null; vendedor = null; vehiculo = null; a = 0;
//                marca = ""; modelo = ""; añoFabricacion = 0; estado = ""; motor = ""; noLLantas = 0; precio = 0;
//                System.out.println(); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarSolicitudesSupervisor(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente(); Supervisor supervisor = new Supervisor();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosSolicitudesSupervisor);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                String user2 = st.nextToken();
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                    ArrayList<ArrayList> listVehiculos = new ArrayList();
                    ArrayList<Object> vehicle;
                    listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                    listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                    listVehiculos.add(SistemaConcesionaria.getListCamion());
                    listVehiculos.add(SistemaConcesionaria.getListTractor());
                    
                    
                    for (int j = 0; j < listVehiculos.size(); j++){
                        vehicle = listVehiculos.get(j);
                        Vehiculo v;
                        for (int i = 0; i < vehicle.size(); i++){
                            v = (Vehiculo)vehicle.get(i);
                            if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                                v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                                v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                                v.getPrecio() == precio)
                            {
                                vehiculo = v;
                            }
                        }
                    }
                
                int b = 0;
                for (int i = 0; i < SistemaConcesionaria.getListSupervisor().size(); i++){
                    String usuario = SistemaConcesionaria.getListSupervisor().get(i).getUsername();
                    if (usuario.equals(user2)){
                        supervisor = SistemaConcesionaria.getListSupervisor().get(i);
                        b = 1;
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        if (b == 1){
                            cliente.solicitarCompra(cliente, supervisor, vehiculo);
                        }
                    }
                }
                cliente = null; supervisor = null; vehiculo = null; b = 0;
//                marca = ""; modelo = ""; añoFabricacion = 0; estado = ""; motor = ""; noLLantas = 0; precio = 0;
//                System.out.println(); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarSolicitudesCliente(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosSolicitudesCliente);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                String estadoSolicitud = st.nextToken();
                String motivo = st.nextToken();
                if (motivo.equals("''")){
                    motivo = "";
                }else{
                    StringTokenizer st1 = new StringTokenizer(motivo, "_");
                    motivo = "";
                    int tok = st1.countTokens();
                    for (int i = 0; i < tok; i++){
                        String parMotivo = st1.nextToken();
                        motivo = motivo + " " + parMotivo;
                    }
                    
                }
                
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                String tpSolicitud = st.nextToken();
                
                ArrayList<ArrayList> listVehiculos = new ArrayList();
                ArrayList<Object> vehicle;
                listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                listVehiculos.add(SistemaConcesionaria.getListCamion());
                listVehiculos.add(SistemaConcesionaria.getListTractor());

                int a = 0;
                for (int j = 0; j < listVehiculos.size(); j++){
                    vehicle = listVehiculos.get(j);
                    Vehiculo v;
                    for (int i = 0; i < vehicle.size(); i++){
                        v = (Vehiculo)vehicle.get(i);
                        if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                            v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                            v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                            v.getPrecio() == precio)
                        {
                            vehiculo = v;
                            a = 1;
                        }
                    }
                }
                
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        if (a == 1){
                            cliente.agregarSolicitud(estadoSolicitud, motivo, vehiculo, tpSolicitud);
                        }
                    }
                }
                cliente = null; vehiculo = null; a = 0; //estadoSolicitud = ""; motivo = ""; tpSolicitud = "";
//                marca = ""; modelo = ""; añoFabricacion = 0; estado = ""; motor = ""; noLLantas = 0; precio = 0;
//                System.out.println(); 
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarMantenimientoPreventivo(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosMantenimientoPreventivo);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                int distancia = Integer.parseInt(st.nextToken());
                String progreso = st.nextToken();
                float precioFin = Float.parseFloat(st.nextToken());
                
                ArrayList<ArrayList> listVehiculos = new ArrayList();
                ArrayList<Object> vehicle;
                listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                listVehiculos.add(SistemaConcesionaria.getListCamion());
                listVehiculos.add(SistemaConcesionaria.getListTractor());

                int a = 0; int b = 0;
                for (int j = 0; j < listVehiculos.size(); j++){
                    vehicle = listVehiculos.get(j);
                    Vehiculo v;
                    for (int i = 0; i < vehicle.size(); i++){
                        v = (Vehiculo)vehicle.get(i);
                        if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                            v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                            v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                            v.getPrecio() == precio)
                        {
                            vehiculo = v;
                            a = 1;
                        }
                    }
                }
                
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        b = 1;
                    }
                }
                if (a == 1 && b ==1){
                    Mantenimiento.agregarMantePreventivo(cliente, vehiculo, distancia, progreso, precioFin);
                }
                cliente = null; vehiculo = null; a = 0; b = 0; distancia = 0; progreso = ""; precioFin = 0;
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarMantenimientoEmergencia(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosMantenimientoEmergencia);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                int distancia = Integer.parseInt(st.nextToken());
                String progreso = st.nextToken();
                float costo = Float.parseFloat(st.nextToken());
                float precioFin = Float.parseFloat(st.nextToken());
                
                ArrayList<ArrayList> listVehiculos = new ArrayList();
                ArrayList<Object> vehicle;
                listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                listVehiculos.add(SistemaConcesionaria.getListCamion());
                listVehiculos.add(SistemaConcesionaria.getListTractor());
                int a = 0; int b = 0;
                for (int j = 0; j < listVehiculos.size(); j++){
                    vehicle = listVehiculos.get(j);
                    Vehiculo v;
                    for (int i = 0; i < vehicle.size(); i++){
                        v = (Vehiculo)vehicle.get(i);
                        if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                            v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                            v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                            v.getPrecio() == precio)
                        {
                            vehiculo = v;
                            a = 1;
                        }
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        b = 1;
                    }
                }
                if (a == 1 && b ==1){
                    Mantenimiento.agregarManteEmergencia(cliente, vehiculo, distancia, progreso, costo, precioFin);
                }
                cliente = null; vehiculo = null; a = 0; b = 0; costo = 0; distancia = 0; progreso = ""; precioFin = 0;
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarVehiEntregado(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente();
            JefeTaller jefeTaller = new JefeTaller();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosVehiEntregado);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String estadoVehiComprado = st.nextToken();
                
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                String user1 = st.nextToken();
                String user2 = st.nextToken();
                
                ArrayList<ArrayList> listVehiculos = new ArrayList();
                ArrayList<Object> vehicle;
                listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                listVehiculos.add(SistemaConcesionaria.getListCamion());
                listVehiculos.add(SistemaConcesionaria.getListTractor());
                int a = 0; int b = 0; int c = 0;
                for (int j = 0; j < listVehiculos.size(); j++){
                    vehicle = listVehiculos.get(j);
                    Vehiculo v;
                    for (int i = 0; i < vehicle.size(); i++){
                        v = (Vehiculo)vehicle.get(i);
                        if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                            v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                            v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                            v.getPrecio() == precio)
                        {
                            vehiculo = v;
                            a = 1;
                        }
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListJefeTaller().size(); i++){
                    String usuario = SistemaConcesionaria.getListJefeTaller().get(i).getUsername();
                    if (usuario.equals(user1)){
                        jefeTaller = SistemaConcesionaria.getListJefeTaller().get(i);
                        b = 1;
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user2)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        c = 1;
                    }
                }
                if (a == 1 && b == 1 && c == 1){   
                    jefeTaller.agregarVehiComprado(estadoVehiComprado, cliente, vehiculo);
                }
                cliente = null; jefeTaller = null; vehiculo = null; a = 0; b = 0; c = 0;
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void cargarSoliVehiMantenimiento(){
        try{
            //PARA LA CARGA DE ARCHIVO IGUAL SE CREA UN ARCHIVO Y POR ELLO USAMOS TAMBIEN
            //UN TRY CATCH QUE SE ENCARGUE DE CONTROLAR NUESTRA EXCEPCION SI ES QUE LLEGA A HABERLA
            //SE LE ASIGNA UNA RUTA A NUESTRO ARCHIVO QUE ES EL TEXTO QUE USAREMOS COMO BASE DE DATOS
            //DE LA CUAL EXTRAEREMOS LA INFORMACION PARA CARGARLA AL SISTEMA
            //COMENZAMOS A LEER LINEA A LINEA SEPARANDO LA INFORMACION POR TOKENS
            //Y PARA ELLO USAMOS STRING TOKENIZER QUE NOS AYUDA SEPARANDO LA INFORMACION
            //QUE SE ENCUENTRE CON UN ", " SEPARANSOLOS
            //GRABA LA INFORMACION Y PROCEDE A CERRAR EL ARCHIVO
            Cliente cliente = new Cliente();
            JefeTaller jefeTaller = new JefeTaller();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            File ruta = new File(rutaBaseDatosSoliVehiMantenimiento);
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);
            String soli = null;
            while((soli = br.readLine()) != null){
                StringTokenizer st = new StringTokenizer(soli, ", ");
                String user1 = st.nextToken();
                String user2 = st.nextToken();
                
                String marca = st.nextToken();
                String modelo = st.nextToken();
                int añoFabricacion = Integer.parseInt(st.nextToken());
                String estado = st.nextToken();
                String motor = st.nextToken();
                int noLLantas = Integer.parseInt(st.nextToken());
                float precio = Float.parseFloat(st.nextToken());
                
                String tpSolicitud = st.nextToken();
                int distancia = Integer.parseInt(st.nextToken());
                
                
                ArrayList<ArrayList> listVehiculos = new ArrayList();
                ArrayList<Object> vehicle;
                listVehiculos.add(SistemaConcesionaria.getListAutomovil());
                listVehiculos.add(SistemaConcesionaria.getListMotocicleta());
                listVehiculos.add(SistemaConcesionaria.getListCamion());
                listVehiculos.add(SistemaConcesionaria.getListTractor());
                int a = 0; int b = 0; int c = 1;
                for (int j = 0; j < listVehiculos.size(); j++){
                    vehicle = listVehiculos.get(j);
                    Vehiculo v;
                    for (int i = 0; i < vehicle.size(); i++){
                        v = (Vehiculo)vehicle.get(i);
                        if (v.getMarca().equals(marca) && v.getModelo().equals(modelo) && 
                            v.getAñoFabricacion() == añoFabricacion && v.getEstado().equals(estado) &&
                            v.getMotor().equals(motor) && v.getNoLLantas() == noLLantas &&
                            v.getPrecio() == precio)
                        {
                            vehiculo = v;
                            a = 1;
                        }
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListCliente().size(); i++){
                    String usuario = SistemaConcesionaria.getListCliente().get(i).getUsername();
                    if (usuario.equals(user1)){
                        cliente = SistemaConcesionaria.getListCliente().get(i);
                        b = 1;
                    }
                }
                for (int i = 0; i < SistemaConcesionaria.getListJefeTaller().size(); i++){
                    String usuario = SistemaConcesionaria.getListJefeTaller().get(i).getUsername();
                    if (usuario.equals(user2)){
                        jefeTaller = SistemaConcesionaria.getListJefeTaller().get(i);
                        c = 1;
                    }
                }
                if (a == 1 && b ==1 && c == 1){
                    ArrayList<Object> list = new ArrayList();
                    list.add(cliente); list.add(jefeTaller); list.add(vehiculo); list.add(tpSolicitud); list.add(distancia);
                    jefeTaller.agregarSoliVehiMantenimiento(list);
                }
                cliente = null; vehiculo = null; a = 0; b = 0; c = 0; tpSolicitud = ""; jefeTaller = null;
            }
            br.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarMantenimientoPreventivo(ArrayList<ArrayList> listPreventivo){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosMantenimientoPreventivo);
            pw = new PrintWriter(fw);
            
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            int distancia = 0; float precioFin = 0; String progreso = "";
            
            for (ArrayList<Object> obj : listPreventivo){
                for (int i = 0; i < obj.size(); i++){
                    if (i == 0){
                        cliente = (Cliente)obj.get(i);
                    }
                    if (i == 1){
                        vehiculo = (Vehiculo)obj.get(i);
                    }
                    if (i == 2){
                        distancia = (int)obj.get(i);
                    }
                    if (i == 3){
                        progreso = (String)obj.get(i);
                    }
                    if (i == 4){
                        precioFin = (float)obj.get(i);
                    }
                }
                pw.println(String.valueOf(cliente.getUsername() + ", "
                + vehiculo.getMarca() + ", "
                + vehiculo.getModelo() + ", "
                + vehiculo.getAñoFabricacion() + ", "
                + vehiculo.getEstado() + ", "
                + vehiculo.getMotor() + ", "
                + vehiculo.getNoLLantas() + ", "
                + vehiculo.getPrecio() + ", "
                + distancia + ", "
                + progreso + ", "
                + precioFin
                ));
                cliente = null; vehiculo = null; distancia = 0; progreso = ""; precioFin = 0;
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarMantenimientoEmergencia(ArrayList<ArrayList> listEmergencia){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosMantenimientoEmergencia);
            pw = new PrintWriter(fw);
            
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            int distancia = 0; float precioFin = 0; float costo = 0; String progreso = "";
            
            for (ArrayList<Object> obj : listEmergencia){
                for (int i = 0; i < obj.size(); i++){
                    if (i == 0){
                        cliente = (Cliente)obj.get(i);
                    }
                    if (i == 1){
                        vehiculo = (Vehiculo)obj.get(i);
                    }
                    if (i == 2){
                        distancia = (int)obj.get(i);
                    }
                    if (i == 3){
                        progreso = (String)obj.get(i);
                    }
                    if (i == 4){
                        costo = (float)obj.get(i);
                    }
                    if (i == 5){
                        precioFin = (float)obj.get(i);
                    }
                }
                pw.println(String.valueOf(cliente.getUsername() + ", "
                + vehiculo.getMarca() + ", "
                + vehiculo.getModelo() + ", "
                + vehiculo.getAñoFabricacion() + ", "
                + vehiculo.getEstado() + ", "
                + vehiculo.getMotor() + ", "
                + vehiculo.getNoLLantas() + ", "
                + vehiculo.getPrecio() + ", "
                + distancia + ", "
                + progreso + ", "
                + costo + ", "
                + precioFin
                ));
                cliente = null; vehiculo = null; distancia = 0; progreso = ""; precioFin = 0; costo = 0;
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarVehiEntregado(ArrayList<ArrayList> listVehiEntregado){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosVehiEntregado);
            pw = new PrintWriter(fw);
            
            Cliente cliente = new Cliente();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            String estado = "";
            
            for (ArrayList<Object> obj : listVehiEntregado){
                for (int i = 0; i < obj.size(); i++){
                    if (i == 0){
                        estado = (String)obj.get(i);
                    }
                    if (i == 1){
                        vehiculo = (Vehiculo)obj.get(i);
                    }
                    if (i == 2){
                        cliente = (Cliente)obj.get(i);
                    }
                }
                pw.println(String.valueOf(estado + ", "
                + vehiculo.getMarca() + ", "
                + vehiculo.getModelo() + ", "
                + vehiculo.getAñoFabricacion() + ", "
                + vehiculo.getEstado() + ", "
                + vehiculo.getMotor() + ", "
                + vehiculo.getNoLLantas() + ", "
                + vehiculo.getPrecio() + ", "
                + SistemaConcesionaria.getListJefeTaller().get(0).getUsername() + ", "
                + cliente.getUsername()                
                ));
                cliente = null; vehiculo = null; estado = "";
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarSoliVehiMantenimiento(ArrayList<ArrayList> listSoliVehiMantenimiento){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosSoliVehiMantenimiento);
            pw = new PrintWriter(fw);
            Cliente cliente = new Cliente();
            JefeTaller jefeTaller = new JefeTaller();
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            String tpSolicitud = "";
            
            for (ArrayList<Object> obj : listSoliVehiMantenimiento){
                for (int i = 0; i < obj.size(); i++){
                    if (i == 0){
                        cliente = (Cliente)obj.get(i);
                    }
                    if (i == 1){
                        jefeTaller = (JefeTaller)obj.get(i);
                    }
                    if (i == 2){
                        vehiculo = (Vehiculo)obj.get(i);
                    }
                    if (i == 3){
                        tpSolicitud = (String)obj.get(i);
                    }
                }
                pw.println(String.valueOf(cliente.getUsername() + ", "
                + jefeTaller.getUsername() + ", "
                + vehiculo.getMarca() + ", "
                + vehiculo.getModelo() + ", "
                + vehiculo.getAñoFabricacion() + ", "
                + vehiculo.getEstado() + ", "
                + vehiculo.getMotor() + ", "
                + vehiculo.getNoLLantas() + ", "
                + vehiculo.getPrecio() + ", "
                + tpSolicitud          
                ));
                cliente = null; vehiculo = null; jefeTaller = null; tpSolicitud = "";
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarSolicitudesCliente(ArrayList<Cliente> cliente){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosSolicitudesCliente);
            pw = new PrintWriter(fw);
            
            ArrayList<ArrayList> listSolicitudes = new ArrayList();
            String estado = ""; String motivo = ""; 
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            String tpSolicitud = "";
            
            
            for (Cliente cli : cliente){
                listSolicitudes = cli.getSolicitud();
                for (ArrayList solicitud : listSolicitudes){
                    for (int i = 0; i < solicitud.size(); i++){
                        if (i == 0){
                            estado = (String)solicitud.get(i);
                        }
                        if (estado.equals("Aceptada")){
                            motivo = "''";
                        }else{
                            if (i == 1){
                                motivo = (String)solicitud.get(i);
                            }
                        }
                        if (i == 2){
                            vehiculo = (Vehiculo)solicitud.get(i);
                        }
                        if (i == 3){
                            tpSolicitud = (String)solicitud.get(i);
                        }
                    }
                    pw.print(String.valueOf(cli.getUsername() + ", "
                            + estado + ", " ));
//                            + motivo + ", " 
                            StringTokenizer st = new StringTokenizer(motivo, " ");
                            int tok = st.countTokens();
                            for (int i = 0; i < tok; i++){
                                String parMotivo = st.nextToken();
                                if (i == 0){
                                    pw.print(String.valueOf(parMotivo));
                                }
                                if (i > 0){
                                    pw.print(String.valueOf("_" + parMotivo));
                                }
                                
                            }
                    pw.println(String.valueOf(", " + vehiculo.getMarca() + ", "  
                            + vehiculo.getModelo() + ", " 
                            + vehiculo.getAñoFabricacion() + ", "
                            + vehiculo.getEstado() + ", " 
                            + vehiculo.getMotor() + ", " 
                            + vehiculo.getNoLLantas() + ", " 
                            + vehiculo.getPrecio() + ", " 
                            + tpSolicitud
                    ));
                    estado = ""; motivo = ""; tpSolicitud = ""; cliente = null;
                    vehiculo = null;
                }
            }
            
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarSolicitudesVendedor(ArrayList<Vendedor> vendedor){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosSolicitudesVendedor);
            pw = new PrintWriter(fw);
            
            ArrayList<ArrayList> listSolicitudes = new ArrayList();
            Cliente cliente = new Cliente(); 
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            String tpSolicitud = "";
            
            
            for (Vendedor ven : vendedor){
                listSolicitudes = ven.getSolicitudCotizacion();
                for (ArrayList solicitud : listSolicitudes){
                    for (int i = 0; i < solicitud.size(); i++){
                        if (i == 0){
                            cliente = (Cliente)solicitud.get(i);
                        }
                        if (i == 2){
                            vehiculo = (Vehiculo)solicitud.get(i);
                        }
                    }
                    pw.println(String.valueOf(cliente.getUsername() + ", "
                            + ven.getUsername() + ", "
                            + vehiculo.getMarca() + ", "  
                            + vehiculo.getModelo() + ", " 
                            + vehiculo.getAñoFabricacion() + ", "
                            + vehiculo.getEstado() + ", " 
                            + vehiculo.getMotor() + ", " 
                            + vehiculo.getNoLLantas() + ", " 
                            + vehiculo.getPrecio()
                    ));
                    cliente = null; vehiculo = null;
                }
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarSolicitudesSupervisor(ArrayList<Supervisor> supervisor){
        try{
            FileWriter fw;
            PrintWriter pw;
            fw = new FileWriter(rutaBaseDatosSolicitudesSupervisor);
            pw = new PrintWriter(fw);
            
            ArrayList<ArrayList> listSolicitudes = new ArrayList();
            Cliente cliente = new Cliente(); 
            Vehiculo vehiculo = new Vehiculo("", "", 0, "", "", 0, 0);
            String tpSolicitud = "";
            
            
            for (Supervisor sup : supervisor){
                listSolicitudes = sup.getSolicitudCompra();
                for (ArrayList solicitud : listSolicitudes){
                    for (int i = 0; i < solicitud.size(); i++){
                        if (i == 0){
                            cliente = (Cliente)solicitud.get(i);
                        }
                        if (i == 2){
                            vehiculo = (Vehiculo)solicitud.get(i);
                        }
                    }
                    pw.println(String.valueOf(cliente.getUsername() + ", "
                            + sup.getUsername() + ", "
                            + vehiculo.getMarca() + ", "  
                            + vehiculo.getModelo() + ", " 
                            + vehiculo.getAñoFabricacion() + ", "
                            + vehiculo.getEstado() + ", " 
                            + vehiculo.getMotor() + ", " 
                            + vehiculo.getNoLLantas() + ", " 
                            + vehiculo.getPrecio()
                        ));
                    cliente = null; vehiculo = null;
                }
            }
            pw.close();
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
    public static void grabarDatos(){
        //EN ESTE METODO LLAMAMOS A LA CLASE QUE USAMOS COMO BASE DE DATOS
        //PARA EXTRAER TODA LA INFORMACION DEL SISTEMA Y COMENZAR A GUARDARLA UNA A UNA
        
        ArrayList<Cliente> lCliente = SistemaConcesionaria.getListCliente();
        ArrayList<Vendedor> lVendedor = SistemaConcesionaria.getListVendedor();
        ArrayList<Supervisor> lSupervisor = SistemaConcesionaria.getListSupervisor();
        ArrayList<JefeTaller> lJefeTaller = SistemaConcesionaria.getListJefeTaller();
        ArrayList<Automovil> lAutomovil = SistemaConcesionaria.getListAutomovil();
        ArrayList<Motocicleta> lMotocicleta = SistemaConcesionaria.getListMotocicleta();
        ArrayList<Camion> lCamion = SistemaConcesionaria.getListCamion();
        ArrayList<Tractor> lTractor = SistemaConcesionaria.getListTractor();
        ArrayList<ArrayList> lMantenimientoPreventivo = Mantenimiento.getListMantenimientoPreventivo();
        ArrayList<ArrayList> lMantenimientoEmergencia = Mantenimiento.getListMantenimientoEmergencia();
        
        
        grabarCliente(lCliente);
        grabarVendedor(lVendedor);
        grabarSupervisor(lSupervisor);
        grabarJefeTaller(lJefeTaller);
        grabarAutomovil(lAutomovil);
        grabarMotocicleta(lMotocicleta);
        grabarCamion(lCamion);
        grabarTractor(lTractor);
        
        grabarSolicitudesCliente(lCliente);
        grabarSolicitudesVendedor(lVendedor);
        grabarSolicitudesSupervisor(lSupervisor);
        grabarVehiEntregado(lJefeTaller.get(0).getVehiEntregado());
        grabarSoliVehiMantenimiento(lJefeTaller.get(0).getSoliVehiMantenimiento());
        grabarMantenimientoPreventivo(lMantenimientoPreventivo);
        grabarMantenimientoEmergencia(lMantenimientoEmergencia);
        
        System.out.println("-----------------------------------------------------------------" + "\n"
                         + "GRACIAS POR USAR EL SISTEMA DE LA CONCESIONARIA. VUELVA PRONTO!!!" + "\n"
                         + "-----------------------------------------------------------------");
    }
}
