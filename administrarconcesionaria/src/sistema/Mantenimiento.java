/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema;
import java.util.ArrayList;
import usuarios.*;
import vehiculos.*;
/**
 *
 * @author CltControl
 */
public abstract class  Mantenimiento {
    private static ArrayList<ArrayList> ListMantenimientoPreventivo= new ArrayList();
    private static ArrayList<ArrayList> ListMantenimientoEmergencia= new ArrayList();

    //GETTERS
    public static ArrayList<ArrayList> getListMantenimientoPreventivo() {
        return ListMantenimientoPreventivo;
    }
    public static ArrayList<ArrayList> getListMantenimientoEmergencia() {
        return ListMantenimientoEmergencia;
    }
   public static void agregarMantePreventivo(Cliente cliente, Vehiculo vehiculo, int distancia, String progreso, float precioFin){
        ArrayList<Object> list = new ArrayList();
        list.add(cliente);
        list.add(vehiculo);
        list.add(distancia);
        list.add(progreso);
         list.add(precioFin);
        ListMantenimientoPreventivo.add(list);
   }
    
    public static void agregarManteEmergencia(Cliente cliente, Vehiculo vehiculo,int distancia, String progreso,  float costo, float precioFin){
        ArrayList<Object> list = new ArrayList();
        list.add(cliente);
        list.add(vehiculo);
        list.add(distancia);
        list.add(progreso);
        list.add(costo);
        list.add(precioFin);
        ListMantenimientoEmergencia.add(list);
    }
}
    

