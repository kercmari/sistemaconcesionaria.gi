/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Carlos
 */
public class Camion extends Vehiculo{
    
    //ATRIBUTOS
    private float capacidadCarga;
    private int noEje;
    
    //CONSTRUCTOR
    public Camion(){
        super("", "", 0, "", "", 0, 0);
    }
    public Camion(String marca, String modelo, int añoFabricacion, String estado, String motor, int noLLantas, float precio, float capacidadCarga, int noEje){
        super(marca, modelo, añoFabricacion, estado, motor, noLLantas, precio);
        this.capacidadCarga = capacidadCarga;
        this.noEje = noEje;
    }
    
    //GETTERS
    public float getCapacidadCarga(){
        return this.capacidadCarga;
    }
    public int getNoEje(){
        return this.noEje;
    }
    
    //SETTERS
    public void setCapacidadCarga(float capacidadCarga){
        this.capacidadCarga = capacidadCarga;
    }
    public void setNoEje(int noEje){
        this.noEje = noEje;
    }
    
    //METODOS
    @Override
    //Presentar la informacion de camion
    public String toString(){
        return ("-----=====Camión=====-----" + "\n" +
                "Marca: " + this.getMarca() + 
                ", Modelo: " +  this.getModelo() +
                ", Año Fabricación: " + this.getAñoFabricacion() + 
                ", Estado: " + this.getEstado() + 
                ", Motor: " + this.getMotor() + 
                ", No. LLantas: " + this.getNoLLantas() + 
                ", Precio: " + this.getPrecio() + 
                ", Capacidad de Carga: " + this.capacidadCarga + " Kilo(s)" + 
                ", No. Eje: " + this.noEje + 
                "\n");
    }
}
