/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Carlos
 */
public class Automovil extends Vehiculo{
    
    //ATRIBUTOS
    private int noAsiento;
    private boolean convertible;
    private boolean camaraRetro;
    
    //CONSTRUCTOR
    public Automovil(){
        super("","",0,"","",0,0);
    }
    public Automovil(String marca, String modelo, int añoFabricacion, String estado, String motor, int noLLantas, float precio, int noAsiento, boolean convertible, boolean camaraRetro){
        super(marca, modelo, añoFabricacion, estado, motor, noLLantas, precio);
        this.noAsiento = noAsiento;
        this.convertible = convertible;
        this.camaraRetro = camaraRetro;
    }
    
    //GETTERS
    public int getNoAsiento() {
        return this.noAsiento;
    }
    public boolean getConvertible() {
        return this.convertible;
    }
    public boolean getCamaraRetro() {
        return this.camaraRetro;
    }
    
    //SETTERS
    public void setNoAsiento(int noAsiento) {
        this.noAsiento = noAsiento;
    }
    public void setConvertible(boolean convertible) {
        this.convertible = convertible;
    }
    public void setCamaraRetro(boolean camaraRetro) {
        this.camaraRetro = camaraRetro;
    }
    
    //METODOS
    @Override
    //Presentar la informacion de automovil
    public String toString(){
        return ("-----=====Automovil=====-----" + "\n" +
                "Marca: " + this.getMarca() + 
                ", Modelo: " +  this.getModelo() +
                ", Año Fabricación: " + this.getAñoFabricacion() + 
                ", Estado: " + this.getEstado() + 
                ", Motor: " + this.getMotor() + 
                ", No. LLantas: " + this.getNoLLantas() + 
                ", Precio: " + this.getPrecio() + 
                ", No. Asientos: " + this.noAsiento + 
                ", Convertible: " + this.convertible + 
                ", Cámara Retro: " + this.camaraRetro + 
                "\n");
    }
    

    
    
    
}
